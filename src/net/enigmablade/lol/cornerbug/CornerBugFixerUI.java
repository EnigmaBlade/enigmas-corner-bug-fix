package net.enigmablade.lol.cornerbug;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class CornerBugFixerUI
{
	private CornerBugFixer main;
	
	private SystemTray systemTray;
	private TrayIcon trayIcon;
	
	private JPopupMenu popup;
	
	public CornerBugFixerUI(CornerBugFixer app)
	{
		main = app;
		
		initSystemTray();
	}
	
	private void initSystemTray()
	{
		if(SystemTray.isSupported())
		{
			try
			{
				systemTray = SystemTray.getSystemTray();
			}
			catch(Exception e)
			{
				System.out.println("Error: Failed to get the system tray");
				e.printStackTrace();
			}
			
			//Create pop-up menu
			popup = new JPopupMenu();
			
			JMenuItem titleLabel = new JMenuItem(main.appName);
			titleLabel.setEnabled(false);
			popup.add(titleLabel);
			
			popup.addSeparator();
			
			JMenuItem exitMenuItem = new JMenuItem("Exit");
			exitMenuItem.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					main.close();
				}
			});
			popup.add(exitMenuItem);
			
			//Create tray icon
			Image trayImage = new ImageIcon(getClass().getClassLoader().getResource("resources/icon.png")).getImage();
			trayIcon = new TrayIcon(trayImage, main.appName, null);
			trayIcon.setImageAutoSize(true);
			trayIcon.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseReleased(MouseEvent e)
				{
					if(e.isPopupTrigger())
					{
						//menuShown = !menuShown;
						
						int height = (2*25)+(1*5);
						int yLoc = e.getY()-height;
						int scrHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
						if(yLoc > scrHeight)
							yLoc = scrHeight-yLoc;
						popup.setLocation(e.getX(), e.getY()-height);
						popup.setInvoker(popup);
						popup.setVisible(true);
					}
				}
			});
			
			try
			{
				systemTray.add(trayIcon);
			}
			catch(AWTException e)
			{
				System.out.println("Error: Failed to initialize system tray");
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Error: OS/Java installation does not support the system tray");
		}
	}
	
	public void destroy()
	{
		
	}
}
