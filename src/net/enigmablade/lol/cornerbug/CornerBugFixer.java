package net.enigmablade.lol.cornerbug;

public class CornerBugFixer
{
	public final String appName = "Corner Bug Fixer";
	
	private CornerBugFixerUI ui;
	private WindowMaximizer maxer;
	
	public CornerBugFixer(int waitTime)
	{
		if(waitTime < 0)
			waitTime = 1500;
		
		ui = new CornerBugFixerUI(this);
		maxer = new WindowMaximizer(waitTime);
	}
	
	public void close()
	{
		ui.destroy();
		maxer.destroy();
		System.exit(0);
	}
}
