package net.enigmablade.lol.cornerbug;

import java.util.concurrent.atomic.*;

import com.sun.jna.*;
import com.sun.jna.win32.*;

public class WindowMaximizer extends Thread
{
	private int waitTime;
	
	private AtomicBoolean running = new AtomicBoolean(true);
	
	private int currentWindow = 0;
	
	public WindowMaximizer(int waitTime)
	{
		this.waitTime = waitTime;
		
		start();
	}
	
	public static interface User32 extends StdCallLibrary
	{
		final int GW_HWNDNEXT = 2;
		final int SW_MAXIMIZE = 3;
		
		final User32 instance = (User32)Native.loadLibrary("user32", User32.class);
		
		int GetWindowRect(int hWnd, WindowRect r);
		
		void GetWindowTextA(int hWnd, byte[] buffer, int buflen);
		
		int GetForegroundWindow();
		
		boolean ShowWindow(int hWnd, int nCmdShow);
		
	}
	
	public static class WindowRect extends Structure implements Comparable<WindowRect>
	{
		public int left, top, right, bottom;
		
		@Override
		public int compareTo(WindowRect o)
		{
			if(left == o.left && top == o.top && right == o.right && bottom == o.bottom)
				return 0;
			else
				return 1;
		}
	}
	
	@Override
	public void run()
	{
		while(running.get())
		{
			int fgWin = User32.instance.GetForegroundWindow();
			if(fgWin != currentWindow)
			{
				currentWindow = fgWin;
				byte[] buffer = new byte[1024];
				User32.instance.GetWindowTextA(currentWindow, buffer, buffer.length);
				String title = Native.toString(buffer);
				if(title.toLowerCase().equals("league of legends (tm) client"))
				{
					WindowRect rect = new WindowRect();
					User32.instance.GetWindowRect(fgWin, rect);
					System.out.println("LoL window: left="+rect.left+", right="+rect.right+", top="+rect.top+", bot="+rect.bottom);
					if(rect.top != 0 && rect.left != 0 && (rect.bottom-rect.top) > 300 && (rect.right-rect.left) > 300)
					{
						boolean success = User32.instance.ShowWindow(fgWin, User32.SW_MAXIMIZE);
						System.out.println("Maximized: "+success);
					}
					else
					{
						System.out.println("LoL window not valid");
						currentWindow = -1;
					}
				}
			}
			
			try
			{
				sleep(waitTime);
			}
			catch(InterruptedException ex){}
		}
	}
	
	public void destroy()
	{
		running.set(false);
	}
}
