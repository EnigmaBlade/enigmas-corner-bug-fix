package net.enigmablade.lol.cornerbug;

import java.io.*;
import javax.swing.*;

public class CornerBugFixerMain
{
	public static void main(String[] args)
	{
		try
		{
			PrintStream log = new PrintStream("log.txt");
			System.setOut(log);
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e){}
		
		int time = -1;
		for(String s : args)
		{
			try
			{
				time = Integer.parseInt(s);
			}
			catch(NumberFormatException e){}
		}
		
		new CornerBugFixer(time);
	}
}
