package net.enigmablade.lol.cornerbug.test;

import java.awt.*;

import javax.swing.*;

public class TestWindow
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("league of legends (tm) client");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setVisible(true);
	}
}
